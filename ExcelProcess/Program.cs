﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;

namespace ExcelProcess
{
    class Program
    {
        static void Main(string[] args)
        {
            test();
        }

        private static void test()
        {
            IWorkbook workbook = null;  //新建IWorkbook物件
            string filePath = "D:\\Projects\\ExcelProcess\\ExcelProcess\\收據格式.xlsx";
            string newPath = "D:\\test.xlsx";
            using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                workbook = new XSSFWorkbook(file);
            }

            //ISheet sheet = workbook.GetSheetAt(0);
            //IRow row = sheet.GetRow(7);
            //ICell cell = row.GetCell(2);
            //cell.SetCellValue("中華民國");
            workbook.GetSheetAt(0).GetRow(5).GetCell(2).SetCellValue("中華民國 109年9月30日");
            workbook.GetSheetAt(0).GetRow(6).GetCell(1).SetCellValue(1234567890.ToString("N0"));


            using (FileStream fs = File.OpenWrite(newPath))
            {
                workbook.Write(fs);
            }
            workbook.Close();
        }


    }
}
